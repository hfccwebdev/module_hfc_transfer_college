<?php

/**
 * @file
 * Contains transfer_college.page.inc.
 *
 * Page callback for Transfer college entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Transfer college templates.
 *
 * Default template: transfer_college.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_transfer_college(array &$variables) {
  // Fetch TransferCollege Entity Object.
  $transfer_college = $variables['elements']['#transfer_college'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
