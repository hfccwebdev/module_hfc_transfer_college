<?php

/**
 * @file
 * Contains post-update hooks for the hfc_transfer_college module.
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_post_update_NAME().
 */
function hfc_transfer_college_post_update_make_revisionable(&$sandbox) {

  $definition_update_manager = \Drupal::entityDefinitionUpdateManager();
  $last_installed_schema_repository = \Drupal::service('entity.last_installed_schema.repository');

  $entity_type = $definition_update_manager->getEntityType('transfer_college');
  $field_storage_definitions = $last_installed_schema_repository->getLastInstalledFieldStorageDefinitions('transfer_college');

  // Update the entity type definition.
  $entity_keys = $entity_type->getKeys();
  $entity_keys['revision'] = 'revision_id';
  $entity_type->set('entity_keys', $entity_keys);
  $entity_type->set('revision_table', 'hfc_transfer_college_revisions');

  $revision_metadata_keys = [
    'revision_default' => 'revision_default',
  ];
  $entity_type->set('revision_metadata_keys', $revision_metadata_keys);

  // Update existing field definitions.
  $field_storage_definitions['name']->setRevisionable(TRUE);

  $field_storage_definitions['id'] = BaseFieldDefinition::create('integer')
    ->setName('id')
    ->setTargetEntityTypeId('transfer_college')
    ->setLabel(new TranslatableMarkup('ID'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE);

  // Add new fields.
  $field_storage_definitions['revision_id'] = BaseFieldDefinition::create('integer')
    ->setName('revision_id')
    ->setTargetEntityTypeId('transfer_college')
    ->setTargetBundle(NULL)
    ->setLabel(new TranslatableMarkup('Revision ID'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE);

  $field_storage_definitions['revision_default'] = BaseFieldDefinition::create('boolean')
    ->setName('revision_default')
    ->setTargetEntityTypeId('transfer_college')
    ->setTargetBundle(NULL)
    ->setLabel(new TranslatableMarkup('Default revision'))
    ->setDescription(new TranslatableMarkup('A flag indicating whether this was a default revision when it was saved.'))
    ->setStorageRequired(TRUE)
    ->setInternal(TRUE)
    ->setTranslatable(FALSE)
    ->setRevisionable(TRUE);

  $field_storage_definitions['revision_created'] = BaseFieldDefinition::create('created')
    ->setName('revision_created')
    ->setTargetEntityTypeId('transfer_college')
    ->setTargetBundle(NULL)
    ->setLabel(new TranslatableMarkup('Revision create time'))
    ->setDescription(new TranslatableMarkup('The time that the current revision was created.'))
    ->setRevisionable(TRUE);

  $field_storage_definitions['revision_user'] = BaseFieldDefinition::create('entity_reference')
    ->setName('revision_user')
    ->setTargetEntityTypeId('transfer_college')
    ->setTargetBundle(NULL)
    ->setLabel(new TranslatableMarkup('Revision user'))
    ->setDescription(new TranslatableMarkup('The user ID of the author of the current revision.'))
    ->setSetting('target_type', 'user')
    ->setRevisionable(TRUE);

  $field_storage_definitions['revision_log_message'] = BaseFieldDefinition::create('string_long')
    ->setName('revision_log_message')
    ->setTargetEntityTypeId('transfer_college')
    ->setTargetBundle(NULL)
    ->setLabel(new TranslatableMarkup('Revision log message'))
    ->setDescription(new TranslatableMarkup('Briefly describe the changes you have made.'))
    ->setRevisionable(TRUE)
    ->setDefaultValue('');

  // Execute the update.
  $definition_update_manager->updateFieldableEntityType($entity_type, $field_storage_definitions, $sandbox);
  return t('Transfer colleges have been converted to be revisionable.');

}
