<?php

namespace Drupal\hfc_transfer_college\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\hfc_transfer_college\TransferCollegeInterface;

/**
 * Defines the Transfer college entity.
 *
 * @ingroup hfc_transfer_college
 *
 * @ContentEntityType(
 *   id = "transfer_college",
 *   label = @Translation("Transfer college"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\hfc_transfer_college\TransferCollegeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\hfc_transfer_college\Form\TransferCollegeForm",
 *       "add" = "Drupal\hfc_transfer_college\Form\TransferCollegeForm",
 *       "edit" = "Drupal\hfc_transfer_college\Form\TransferCollegeForm",
 *       "delete" = "Drupal\hfc_transfer_college\Form\TransferCollegeDeleteForm",
 *     },
 *     "access" = "Drupal\hfc_transfer_college\TransferCollegeAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\hfc_transfer_college\TransferCollegeHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "hfc_transfer_colleges",
 *   revision_table = "hfc_transfer_college_revisions",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer transfer colleges",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "name",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/transfer-college/{transfer_college}",
 *     "add-form" = "/transfer-college/add",
 *     "edit-form" = "/transfer-college/{transfer_college}/edit",
 *     "delete-form" = "/transfer-college/{transfer_college}/delete",
 *     "version-history" = "/transfer-college/{transfer_college}/revisions",
 *     "revision" = "/transfer-college/{transfer_college}/revisions/{transfer_college_revision}/view",
 *   },
 *   field_ui_base_route = "transfer_college.settings"
 * )
 */
class TransferCollege extends RevisionableContentEntityBase implements TransferCollegeInterface {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Name'))
      ->setDescription(new TranslatableMarkup('The name of the Transfer college entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 191,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['revision_default'] = BaseFieldDefinition::create('boolean')
      ->setName('revision_default')
      ->setLabel(new TranslatableMarkup('Default revision'))
      ->setDescription(new TranslatableMarkup('A flag indicating whether this was a default revision when it was saved.'))
      ->setStorageRequired(TRUE)
      ->setInternal(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(TRUE);

    return $fields;
  }

}
