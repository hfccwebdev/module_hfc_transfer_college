<?php

namespace Drupal\hfc_transfer_college;

/**
 * Defines the HFC Transfer College service interface.
 */
interface TransferCollegeServiceInterface {

  /**
   * Defines the configuration name for this module.
   */
  public const SETTINGS = 'hfc_transfer_college.settings';

  /**
   * Resets field_partner_display_flag based on related articulation agreements.
   */
  public function resetPartnerDisplayFlags(): void;

}
