<?php

namespace Drupal\hfc_transfer_college;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;

/**
 * Defines the HFC Transfer College service.
 */
class TransferCollegeService implements TransferCollegeServiceInterface {

  /**
   * Stores the Current user information.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  /**
   * Stores the Date Formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Current User information.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function resetPartnerDisplayFlags(): void {
    $tcrepo = $this->entityTypeManager->getStorage('transfer_college');
    $noderepo = $this->entityTypeManager->getStorage('node');

    $ids = $tcrepo->getQuery()->accessCheck(FALSE)->sort('name')->execute();

    $colleges = $tcrepo->loadMultiple($ids);

    $today = $this->dateFormatter->format($this->time->getRequestTime(), 'custom', 'Y-m-d');

    foreach ($colleges as $id => $college) {
      $usage = $noderepo->getQuery()
        ->accessCheck(FALSE)
        ->condition('type', 'articulation_agreement')
        ->condition('status', NodeInterface::PUBLISHED)
        ->condition('field_expiration_date', $today, ">")
        ->condition('field_artic_college', $id)
        ->execute();

      if (empty($college->field_partner_display_flag->value) !== empty($usage)) {
        $college->field_partner_display_flag->value = !empty($usage);
        $college->setNewRevision(TRUE);
        $college->setRevisionCreationTime($this->time->getRequestTime());
        $college->setRevisionUserId($this->currentUser->id());
        $college->setRevisionLogMessage('Update partner display flag.');
        $college->save();
      }
    }
  }

}
