<?php

namespace Drupal\hfc_transfer_college;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Transfer college entities.
 *
 * @ingroup hfc_transfer_college
 */
interface TransferCollegeInterface extends ContentEntityInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Transfer college name.
   *
   * @return string
   *   Name of the Transfer college.
   */
  public function getName();

  /**
   * Sets the Transfer college name.
   *
   * @param string $name
   *   The Transfer college name.
   *
   * @return \Drupal\hfc_transfer_college\TransferCollegeInterface
   *   The called Transfer college entity.
   */
  public function setName($name);

}
