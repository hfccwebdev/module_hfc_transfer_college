<?php

namespace Drupal\hfc_transfer_college\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Transfer college edit forms.
 *
 * @ingroup hfc_transfer_college
 */
class TransferCollegeForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\hfc_transfer_college\Entity\TransferCollege */
    $form = parent::buildForm($form, $form_state);
    $form['revision']['#default_value'] = TRUE;
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $entity->setNewRevision();
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Transfer college.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Transfer college.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.transfer_college.canonical', ['transfer_college' => $entity->id()]);
  }

}
