<?php

namespace Drupal\hfc_transfer_college\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hfc_transfer_college\TransferCollegeServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Transfer College Settings form.
 *
 * @package Drupal\hfc_transfer_college\Form
 *
 * @ingroup hfc_transfer_college
 */
class TransferCollegeSettingsForm extends ConfigFormBase {

  /**
   * Stores the Transfer College Service.
   *
   * @var \Drupal\hfc_transfer_college\TransferCollegeServiceInterface
   */
  private $tcService;

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'TransferCollege_settings';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('hfc_transfer_college_service')
    );
  }

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\hfc_transfer_college\TransferCollegeServiceInterface $tcService
   *   The HFC Transfer College service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TransferCollegeServiceInterface $tcService
  ) {
    parent::__construct($config_factory);
    $this->tcService = $tcService;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      TransferCollegeServiceInterface::SETTINGS,
    ];
  }

  /**
   * Defines the settings form for Transfer college entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(TransferCollegeServiceInterface::SETTINGS);

    $form['TransferCollege_settings']['#markup'] = 'Settings form for Transfer college entities. Manage field settings here.';

    $form['reset_partner_flags'] = [
      '#title' => $this->t('Reset partner flags now'),
      '#type' => 'checkbox',
      '#description' => $this->t('Check this box to automatically rebuild partner display flags now.'),
    ];

    $form['cron_enable'] = [
      '#title' => $this->t('Enable cron'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('cron_enable') ?? 0,
      '#description' => $this->t('Check this box to automatically rebuild partner display flags using cron.'),
    ];

    $form['cron_interval'] = [
      '#title' => $this->t('Cron interval'),
      '#type' => 'select',
      '#options' => [
        21600 => $this->t('6 hours'),
        86400 => $this->t('Daily'),
        604800 => $this->t('Weekly'),
      ],
      '#default_value' => $config->get('cron_interval') ?? 86400,
      '#description' => $this->t('Check this box to automatically rebuild partner display flags.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('reset_partner_flags')) {
      // This setting should not be stored.
      // Update flags instead.
      $this->tcService->resetPartnerDisplayFlags();
    }

    $config = $this->configFactory->getEditable(TransferCollegeServiceInterface::SETTINGS);
    foreach (['cron_enable', 'cron_interval'] as $key) {
      $config->set($key, $form_state->getValue($key))->save();
    }
    parent::submitForm($form, $form_state);
  }

}
