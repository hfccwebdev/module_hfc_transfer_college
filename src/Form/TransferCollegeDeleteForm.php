<?php

namespace Drupal\hfc_transfer_college\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Transfer college entities.
 *
 * @ingroup hfc_transfer_college
 */
class TransferCollegeDeleteForm extends ContentEntityDeleteForm {

}
